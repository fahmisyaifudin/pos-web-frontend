export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'pos-client',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { href: '/assets/css/bootstrap.css', rel: 'stylesheet', type: 'text/css'},
      { href: '/assets/css/ui.css', rel: 'stylesheet', type: 'text/css'},
      { href: '/assets/fonts/fontawesome/css/fontawesome-all.min.css', rel: 'stylesheet', type: 'text/css'}
    ],
    script: [
      { src: '/assets/js/jquery-2.0.0.min.js', type: 'text/javascript'},
      { src: '/assets/js/bootstrap.bundle.min.js', type: 'text/javascript'}
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/currency-input.js'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/vuetify'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios'
  ],

  axios: {
    baseURL: process.env.BASE_URL || 'http://localhost:8000',
  },

  env: {
    API_URL: process.env.API_URL || 'http://localhost:8000',
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },
  server: {
    port: 3001, // default: 3000
  }
}
