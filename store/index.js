export const state = () => ({
    isLogin: false,
    token: null,
    user: {},
    transaction: {},
  })
  
export const mutations = {
    successLogin(state, data) {
        state.isLogin = true;
        state.token = data.token;
        state.user = data.user;
    },
    logOut(state){
        state.isLogin= false
        state.token= null
        state.user= {}
    },
    createTrx(state, data){
        state.transaction = {
            ...data.data,
            cash: data.cash
        }
    }
}
  