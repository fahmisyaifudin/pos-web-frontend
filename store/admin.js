export const state = () => ({
    isLogin: false,
    token: null,
    user: {},
  })
  
export const mutations = {
    successLogin(state, data) {
        state.isLogin = true;
        state.token = data.token;
        state.user = data.user;
    },
    logOut(state){
        state.isLogin= false
        state.token= null
        state.user= {}
    }
}
  